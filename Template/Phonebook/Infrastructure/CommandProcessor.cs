﻿using PhonebookApp.Commands;
using PhonebookApp.Models;
using System;

namespace PhonebookApp.Infrastructure
{
    public static class CommandProcessor
    {
        public static string ProcessCommand(string input)
        {
            var args = input.Split();
            ICommand command;
            CommandCheck checker = new CommandCheck();
            command = checker.CommandChecker(args[0]);
            return command.Execute(args);
            
        }
    }
}
