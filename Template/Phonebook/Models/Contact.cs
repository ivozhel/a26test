﻿using System;

namespace PhonebookApp.Models
{
    public class Contact
    {

        public Contact(string name, string phoneNumber)
        {
            this.CreatedOn = DateTime.Now;
            this.Name = name;
            this.PhoneNumber = phoneNumber;
        }

        public DateTime CreatedOn { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }

        public string GetContactInfo()
        {
            return $"{this.Name}: [{this.PhoneNumber}] - Created on {this.CreatedOn.ToString("HH:mm:ss tt")}";
        }
    }
}
