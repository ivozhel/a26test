﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PhonebookApp.Models
{
    public static class Phonebook
    {
        private static readonly List<Contact> contacts = new List<Contact>();

        public static void SeedContacts()
        {
            if (!contacts.Any())
            {
                string json = File.ReadAllText(@"../../../contacts.json");
                var seededContacts = JsonConvert.DeserializeObject<List<Contact>>(json);

                foreach (var contact in seededContacts)
                {
                    contacts.Add(contact);
                }
            }
        }

        public static void AddContact(Contact contact)
        {
            foreach (var contactInList in contacts)
            {
                if (contactInList == contact)
                {
                    throw new ArgumentException("Contact already exists.");
                }
            }
            contacts.Add(contact);
        }
        public static string RemoveContact(string name)
        {
            Contact contactToRemove = null;
            foreach (var contact in contacts)
            {
                if (contact.Name == name)
                {
                    contactToRemove = contact;
                }
            }
            if (contactToRemove == null)
            {
                return $"Contact {name} does not exist!";
            }
            contacts.Remove(contactToRemove);
            return $"Contact removed";
        }
        public static string UpdateContact(string name, string phoneNumber )
        {
            Contact contactToUpdate = null;
            foreach (var contact in contacts)
            {
                if (contact.Name == name)
                {
                    contactToUpdate = contact;
                }
            }
            if (contactToUpdate == null)
            {
                return $"Contact {name} does not exist!";
            }
            contacts.Remove(contactToUpdate);
            contactToUpdate.PhoneNumber = phoneNumber;
            contacts.Add(contactToUpdate);
            return $"Contact updated";
        }
        public static string Search(string startString)
        {
            List<Contact> matchedContacts = new List<Contact>();
            int counter = 0;
            foreach (var contact in contacts)
            {
                for (int i = 0; i < startString.Length; i++)
                {
                    if (contact.Name[i] == startString[i])
                    {
                        counter++;
                    }
                }
                if (counter == startString.Length)
                {
                    matchedContacts.Add(contact);
                }
                counter = 0;
            }
            if (matchedContacts.Count == 0)
            {
                return $"No contacts found";
            }
            return ListContactsReuse(matchedContacts);
        }
        public static string ListContacts(string optionalStr = "def")
        {
            var builder = new StringBuilder();
            if (optionalStr == "name")
            {
                var listSortedByName = contacts.OrderBy(x => x.Name).ToList();
                return ListContactsReuse(listSortedByName);
            }
            if (optionalStr == "time")
            {
                var listSortedByTime = contacts.OrderByDescending(x => x.CreatedOn).ToList();
                return ListContactsReuse(listSortedByTime);
            }
            return ListContactsReuse(contacts);
        }
        private static string ListContactsReuse(List<Contact> list)
        {
            var builder = new StringBuilder();
            foreach (var contact in list)
            {
                builder.AppendLine(contact.GetContactInfo());
            }

            return builder.ToString();
        }
    }
}
