﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhonebookApp.Commands
{
    class InvalidCommand : ICommand
    {
        public string Execute(string[] args)
        {
            return "Invalid command";
        }
    }
}
