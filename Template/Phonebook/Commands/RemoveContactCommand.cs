﻿using PhonebookApp.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhonebookApp.Commands
{
    class RemoveContactCommand : ICommand
    {
        public string Execute(string[] args)
        {
            if (string.IsNullOrEmpty(args[1]))
            {
                throw new ArgumentException("Please provide a contact name");
            }
            return Phonebook.RemoveContact(args[1]);
        }
    }
}
