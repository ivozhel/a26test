﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhonebookApp.Commands
{
    class CommandCheck
    {
        public ICommand CommandChecker(string arg)
        {

            ICommand command;
            switch (arg)
            {
                case "addcontact":
                    command = new AddContactCommand();
                    return command;

                case "removecontact":
                    command = new RemoveContactCommand();
                    return command;

                case "updatecontact":
                    command = new UpdateContactCommand();
                    return command;

                case "search":
                    command = new SearchContactCommand();
                    return command;

                case "listcontacts":
                    command = new ListContactsCommand();
                    return command;
                default:
                    return new InvalidCommand();
            }

        }
    }
}
