﻿using PhonebookApp.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhonebookApp.Commands
{
    class ListContactsCommand : ICommand
    {
        public string Execute(string[] args)
        {
            if (args.Length == 1)
            {
                return Phonebook.ListContacts();
            }
            return Phonebook.ListContacts(args[1]);
        }
    }
}
