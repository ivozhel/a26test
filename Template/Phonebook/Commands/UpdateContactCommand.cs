﻿using PhonebookApp.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhonebookApp.Commands
{
    class UpdateContactCommand : ICommand
    {
        public string Execute(string[] args)
        {
            if (string.IsNullOrEmpty(args[1]))
            {
                throw new ArgumentException("Please provide a contact name");
            }
            return Phonebook.UpdateContact(args[1], args[2]);
        }
    }
}
