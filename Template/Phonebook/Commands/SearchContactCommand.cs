﻿using PhonebookApp.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhonebookApp.Commands
{
    class SearchContactCommand : ICommand
    {
        public string Execute(string[] args)
        {
            if (string.IsNullOrEmpty(args[1]))
            {
                throw new ArgumentException("Please provide something for search");
            }
            return Phonebook.Search(args[1]);
        }
    }
}
