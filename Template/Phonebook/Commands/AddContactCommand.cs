﻿

using PhonebookApp.Models;
using System;

namespace PhonebookApp.Commands
{
    class AddContactCommand : ICommand
    {
       public string Execute (string[] args)
        {
            if (args.Length != 3)
            {
                throw new ArgumentException("Please provide at least 3 arguments. The first argument should be name, the second - phonenumber");
            }

            var contact = new Contact(args[1], args[2]);

            Phonebook.AddContact(contact);

            return $"Created contact " + contact.GetContactInfo();
        }
    }
}
