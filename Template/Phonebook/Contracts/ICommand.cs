﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhonebookApp.Commands
{
    interface ICommand
    {
        string Execute(string[] args);
    }
}
